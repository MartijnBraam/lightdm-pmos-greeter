PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
DATADIR ?= $(PREFIX)/share
CONFDIR = /etc

target/release/lightdm-pmos-greeter: src/main.rs
	cargo build --release
build: target/release/lightdm-pmos-greeter
install: build
	install -Dm755 target/release/lightdm-pmos-greeter -t $(DESTDIR)$(BINDIR)
	install -Dm644 lightdm-pmos-greeter.desktop -t $(DESTDIR)$(DATADIR)/xgreeters
