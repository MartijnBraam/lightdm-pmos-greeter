# Lightdm postmarketOS greeter

This is a fork from lightdm-mobile-greeter from https://raatty.club:3000/raatty/lightdm-mobile-greeter.git to apply
postmarketOS branding.

A simple log in screen for use on touch screens, designed for use on postmarketOS but should work for others too.

## Installing
```
make install
```
## Configuring
As of version 3 I will now try to guess the user.

lightdm.conf
```
greeter-session=lightdm-pmos-greeter
user-session=<THE CHOSEN DE TO LAUNCH AFTER SUCCESSFUL LOGIN>
```
